import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/service/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useDialogStore } from "@/stores/dialog";
export const useProductStore = defineStore("product", () => {
  const dialogStore = useDialogStore();
  const product = ref<Product[]>([]);
  const messageStore = useMessageStore();
  const deleteProductTemp = ref<Product>();
  const confirmDialog = ref(false);
  const loadingStore = useLoadingStore();
  const editedProduct = ref<Product>({ name: "", price: 0 });
  const getProduct = async () => {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      product.value = res.data;
      console.log(res);
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถดึง Product ได้");
    }
    loadingStore.isLoading = false;
  };

  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveUser(editedProduct.value);
      }

      confirmDialog.value = false;
      await getProduct();
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถ Save Product ได้");
    }
    loadingStore.isLoading = false;
    editedProduct.value = { name: "", price: 0 };
  }
  async function deleteProduct() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.deleteProduct(
        deleteProductTemp.value?.id!
      );
      await getProduct();
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถลบ Product ได้");
    }
    loadingStore.isLoading = false;
    dialogStore.dialog = false;
  }
  function clearProduct() {
    editedProduct.value = { name: "", price: 0 };
  }
  function editProduct(product: Product) {
    editedProduct.value = { ...product };
    confirmDialog.value = true;
  }
  return {
    product,
    editProduct,
    getProduct,
    saveProduct,
    deleteProduct,
    editedProduct,
    confirmDialog,
    clearProduct,
    deleteProductTemp,
  };
});
