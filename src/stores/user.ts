import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/user";
import userService from "@/service/user";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useDialogStore } from "./dialog";

export const useUserStore = defineStore("user", () => {
  const user = ref<User[]>([]);
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const dialogStore = useDialogStore();
  const deleteUserTemp = ref<User>();
  const confirmDialog = ref(false);
  const loadingStore = useLoadingStore();
  const editedUser = ref<User>({ username: "", name: "", password: "" });
  const getUser = async () => {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUser();
      user.value = res.data;
      console.log(res);
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถดึง user ได้");
    }
    loadingStore.isLoading = false;
  };

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      clearUser();
    }
  });
  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        const res = await userService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await userService.saveUser(editedUser.value);
      }

      confirmDialog.value = false;
      await getUser();
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถ Save User ได้");
    }
    loadingStore.isLoading = false;
  }
  async function deleteUser() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.deleteUser(deleteUserTemp.value?.id!);
      await getUser();
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถลบ user ได้");
    }
    loadingStore.isLoading = false;
    dialogStore.dialog = false;
  }
  function clearUser() {
    editedUser.value = { username: "", name: "", password: "" };
  }
  function editUser(user: User) {
    editedUser.value = { ...user };
    confirmDialog.value = true;
  }
  return {
    user,
    editUser,
    getUser,
    saveUser,
    dialog,
    deleteUser,
    editedUser,
    confirmDialog,
    deleteUserTemp,
  };
});

