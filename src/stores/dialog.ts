import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import { useProductStore } from "@/stores/product";

export const useDialogStore = defineStore("dialog", () => {
  const dialog = ref(false);
  const sendFrom = ref("");
  const productStore = useProductStore();
  watch(dialog, (newDialog) => {
    if (!newDialog) {
      productStore.clearProduct();
    }
  });
  return { dialog, sendFrom };
});

